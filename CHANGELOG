# Changelog

## Version 2.0.4 (2025-02-10)

- [FIX] String as default serialization method for dumping datetime to json

## Version 2.0.3 (2025-01-13)

- [NEW] Added prototype for scripted events

## Version 2.0.2 (2025-01-08)

- [CHANGE] Changed dtypes of actuators/sensors to float32

## Version 2.0.1 (2025-01-07)

- [FIX] Equalized the boundaries of values at different locations.

## Version 2.0.0 (2024-12-20)

- [CHANGE] Change version to release

## Version 2.0.0rc2 (2024-12-19)

- [CHANGE] Removed `if_necessary` flag from download function

## Version 2.0.0rc1 (2024-12-16)

- [CHANGE] Update readme
- [CHANGE] Preparing for release
- [NEW] Added delta actuators for temperature, radiation, and wind

## Version 2.0.0a2 (2024-09-24)

- [FIX] Changed to use the correct sensor variables

## Version 2.0.0a2 (2024-07-09)

- [CHANGE] Refactored the midas module.py for midas2
- [CHANGE] Refactored the whole package structure 

## Version 1.1.4 (2023-11-29)

* [NEW] Added sun hours and cloudiness to the dataset
* [CHANGE] Refactored internal code
* [NEW] Added key-value logging 

## Verison 1.1.3 (2023-11-17)

* [FIX] Fixed downloading the data and tests

## Version 1.1.2 (2023-11-16)

* [NEW] Added air pressure to the data set

## Version 1.1.1 (2023-10-18)

* [CHANGE] Corrected the space definitions of sensors and actuators

## Version 1.1.0 (2023-07-13)

* [NEW] Added wind speed and wind dir as output

## Version 1.0.1 (2022-09-19)

* [FIX] Space definition caused an error.

## Version 1.0.0 (2022-09-19)

* [NEW] Weather module made to the first 1.0 release
* [NEW] Added sensors for the WeatherCurrent model 
 
## Version 1.0.0rc7 (2022-05-06)

* [NEW] Weather simulator can now be started as external process
  + Set `cmd: cmd` in *weather_params*
  + Weather will log to its own file *midas-weather.log*

## Version 1.0.0rc6 (2022-05-04)

* [CHANGE] Download function is now a class member.

## Version 1.0.0rc5 (2022-04-05)

* [CHANGE] Allow input from time simulator.

## Version 1.0.0rc4 (2022-04-04)

* [NEW] Migrated download function.
* [NEW] Added tests.
* [NEW] Added gitlab-ci.

## Version 1.0.0rc3 (2022-04-01)

* [FIX] Update requirements (really).

## Version 1.0.0rc2 (2022-04-01)

* [CHANGE] Update requirements.

## Version 1.0.0rc1 (2022-03-29)

* [NEW] Moved module to this new package.
